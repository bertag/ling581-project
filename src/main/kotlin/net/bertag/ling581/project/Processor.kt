package net.bertag.ling581.project

import edu.stanford.nlp.pipeline.CoreDocument
import edu.stanford.nlp.pipeline.StanfordCoreNLP
import kotlin.math.pow

class Processor {

    private val pipeline =
        mapOf(
            "annotators" to "tokenize, ssplit, pos, parse, sentiment", "parse.maxlen" to "200",
            // Comment out the next line to use the default CoreNLP model.
            "sentiment.model" to "src/main/resources/model.ser.gz"
        ).toProperties().let { StanfordCoreNLP(it) }

    private val scores = mapOf(
        "Very negative" to 1,
        "Negative" to 2,
        "Neutral" to 3,
        "Positive" to 4,
        "Very Positive" to 5
    )

    fun process(text: String): Double {
        val doc = CoreDocument(text).also { pipeline.annotate(it) }
        // return doc.sentences().map { it.sentiment() }.mapNotNull { scores[it] }.average()
        // return doc.sentences().map { it.sentiment() }.mapNotNull { scores[it] }.median()
        return doc.sentences().map { it.sentiment() }.mapNotNull { scores[it] }.geometricMean()
    }

}

// Derived from https://rosettacode.org/wiki/Averages/Median#Kotlin; Licensed GFDL-1.2.
fun Iterable<Int>.median(): Double {
    return this.map { it.toDouble() }.sorted().let { (it[it.size / 2] + it[(it.size - 1) / 2]) / 2 }
}

// Derived from https://rosettacode.org/wiki/Averages/Pythagorean_means#Kotlin; Licensed GFDL-1.2.
fun Iterable<Int>.geometricMean(): Double {
    return if (count() < 1) Double.NaN
    else this.map { it.toDouble() }.let { it.reduce { n1, n2 -> n1 * n2 }.pow(1.0 / count()) }
}