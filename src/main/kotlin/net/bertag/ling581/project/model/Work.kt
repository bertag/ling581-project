package net.bertag.ling581.project.model

import java.nio.file.Files
import java.nio.file.Path
import java.util.regex.Pattern
import kotlin.streams.asSequence

class Work(val books: List<Book>) {

    val text by lazy { books.joinToString(" ") { it.text } }

    val chapters by lazy {
        books.flatMap { it.chapters }
    }

    val verses by lazy {
        chapters.flatMap { it.verses }
    }

    class Parser(private val file: Path) {

        companion object {
            private val VERSE_PATTERN = Pattern.compile(
                "^(" + Reference.PATTERN.toString() + "). \\d+ (.*)",
                Pattern.DOTALL
            )
        }

        fun parse(): Work {
            val verses =
                Files.lines(file)
                    .asSequence()
                    .map { it.replace(";", ".") }
                    .joinToString("\n")
                    .splitToSequence("\n\n")
                    .filter(::isVerse)
                    .map(::asVerse)

            val contents = mutableListOf<Book>()
            var currentBookName = ""
            var currentBookChapters = mutableListOf<Chapter>()
            var currentChapterVerses = mutableListOf<Verse>()
            var currentChapterRef = ""
            var init = false
            for (verse in verses) {
                val ref = verse.first
                val text = verse.second

                if (ref.verse == 1) {
                    if (init) {
                        currentBookChapters.add(Chapter(currentChapterRef, currentChapterVerses))
                        currentChapterVerses = mutableListOf()
                    }
                    if (ref.chapter == 1) {
                        if (init) {
                            contents.add(Book(currentBookName, currentBookChapters))
                        }
                        currentBookName = ref.book
                        currentBookChapters = mutableListOf()
                    }
                    currentChapterRef = ref.book + " " + ref.chapter
                }
                currentChapterVerses.add(Verse(ref.toString(), verse.second))
                init = true
            }

            // Finishing logic.
            currentBookChapters.add(Chapter(currentChapterRef, currentChapterVerses))
            contents.add(Book(currentBookName, currentBookChapters))

            return Work(contents)
        }

        private fun asVerse(paragraph: String): Pair<Reference, String> {
            val matcher = VERSE_PATTERN.matcher(paragraph).also { it.matches() }
            val reference = Reference.parse(matcher.group(1))
            val text = matcher.group(6).replace("\n", " ")

            return reference to text
        }

        private fun isVerse(paragraph: String): Boolean {
            return VERSE_PATTERN.asPredicate().test(paragraph)
        }

    }

}
