package net.bertag.ling581.project.model

class Verse(val reference: String, val text: String)
