package net.bertag.ling581.project.model

class Chapter(val reference: String, val verses: List<Verse>) {

    val text by lazy { verses.joinToString(" ") { it.text } }

}
