package net.bertag.ling581.project.model

import java.util.regex.Pattern

data class Reference(val book: String, val chapter: Int, val verse: Int) {

    companion object {

        val PATTERN: Pattern = Pattern.compile("(\\d* ?(\\w+\\s?)+) (\\d+):(\\d+)")

        fun parse(reference: String): Reference {
            val matcher = PATTERN.matcher(reference).also { it.matches() }
            val book = matcher.group(1)
            val chapter = matcher.group(3).toInt()
            val verse = matcher.group(4).toInt()

            return Reference(book, chapter, verse)
        }
    }

    override fun toString(): String {
        return "$book $chapter:$verse"
    }

}
