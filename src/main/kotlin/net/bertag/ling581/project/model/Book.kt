package net.bertag.ling581.project.model

data class Book(val reference: String, val chapters: List<Chapter>) {

    val text by lazy { chapters.joinToString(" ") { it.text } }

}
