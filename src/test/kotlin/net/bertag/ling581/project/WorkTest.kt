package net.bertag.ling581.project

import net.bertag.ling581.project.model.Work
import org.junit.jupiter.api.Test
import java.nio.file.Paths

class WorkTest {

    @Test
    fun shouldParseHtml() {
        val file = Paths.get("src/main/resources/bookofmormon.txt")
        val contents = Work.Parser(file).parse()
        println(contents)
    }

}