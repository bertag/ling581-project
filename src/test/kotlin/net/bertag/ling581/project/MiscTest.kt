package net.bertag.ling581.project

import org.junit.jupiter.api.Test
import java.io.BufferedWriter
import java.nio.file.Files
import java.nio.file.Paths

class MiscTest {

    private val processor = Processor()

    @Test
    fun misc() {
        val sentences = listOf(
            //"Now this he spake because of the stiffneckedness of Laman and Lemuel.",
            //"for behold they did murmur in many things against their father, because he was a visionary man, and had led them out of the land of Jerusalem, to leave the land of their inheritance, and their gold, and their silver, and their precious things, to perish in the wilderness.",
            //"And this they said he had done because of the foolish imaginations of his heart."
            "And it came to pass that I, Nephi, being exceedingly young, nevertheless being large in stature, and also having great desires to know of the mysteries of God, wherefore, I did cry unto the Lord.",
            "and behold he did visit me, and did soften my heart that I did believe all the words which had been spoken by my father.",
            "wherefore, I did not rebel against him like unto my brothers."
        )

        for (sentence in sentences) {
            val line = (processor.process(sentence))
            println(line)
        }
    }

    private fun save(writer: BufferedWriter, line: String) {
        println(line)
        writer.write(line)
        writer.newLine()
        writer.flush()
    }

}