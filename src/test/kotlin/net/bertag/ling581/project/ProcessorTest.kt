package net.bertag.ling581.project

import net.bertag.ling581.project.model.Work
import org.junit.jupiter.api.Test
import java.io.BufferedWriter
import java.nio.file.Files
import java.nio.file.Paths

class ProcessorTest {

    companion object {
        private val source = Paths.get("src/main/resources/bookofmormon.txt")
        private val contents = Work.Parser(source).parse()
        private val processor = Processor()
    }

    //@Test
    fun evalVerses() {
        val out = Paths.get("/var/tmp/verse.scores").let { Files.newBufferedWriter(it, Charsets.UTF_8) }
        for (verse in contents.verses) {
            val line = (verse.reference + "\t" + processor.process(verse.text))
            println(line)
            save(out, line)
        }
    }

    @Test
    fun evalChapters() {
        val out = Paths.get("/var/tmp/chapter.scores").let { Files.newBufferedWriter(it, Charsets.UTF_8) }
        for (chapter in contents.chapters) {
            val line = (chapter.reference + "\t" + processor.process(chapter.text))
            save(out, line)
        }
    }

    //@Test
    fun evalBooks() {
        val out = Paths.get("/var/tmp/book.scores").let { Files.newBufferedWriter(it, Charsets.UTF_8) }
        for (book in contents.books) {
            val line = (book.reference + "\t" + processor.process(book.text))
            save(out, line)
        }
    }

    //@Test
    fun evalPartialText() {
        val texts = mapOf(
            "Alma 32:12 (1)" to "I say unto you, it is well that ye are cast out of your synagogues, that ye may be humble, and that ye may learn wisdom. for it is necessary that ye should learn wisdom.",
            "Alma 32:12 (2)" to "For it is because that ye are cast out, that ye are despised of your brethren because of your exceeding poverty, that ye are brought to a lowliness of heart.",
            "Alma 32:12 (3)" to "For ye are necessarily brought to be humble.",
            "Alma 32:12 (all)" to "I say unto you, it is well that ye are cast out of your synagogues, that ye may be humble, and that ye may learn wisdom. for it is necessary that ye should learn wisdom. for it is because that ye are cast out, that ye are despised of your brethren because of your exceeding poverty, that ye are brought to a lowliness of heart. for ye are necessarily brought to be humble.",
            "Alma 62:40-41 (1)" to "And there had been murders, and contentions, and dissensions, and all manner of iniquity among the people of Nephi.",
            "Alma 62:40-41 (2)" to "Nevertheless for the righteous’ sake, yea, because of the prayers of the righteous, they were spared.",
            "Alma 62:40-41 (3)" to "But behold, because of the exceedingly great length of the war between the Nephites and the Lamanites many had become hardened, because of the exceedingly great length of the war.",
            "Alma 62:40-41 (4)" to "And many were softened because of their afflictions, insomuch that they did humble themselves before God, even in the depth of humility.",
            "Alma 62:40-41 (all)" to "And there had been murders, and contentions, and dissensions, and all manner of iniquity among the people of Nephi. nevertheless for the righteous’ sake, yea, because of the prayers of the righteous, they were spared. But behold, because of the exceedingly great length of the war between the Nephites and the Lamanites many had become hardened, because of the exceedingly great length of the war. and many were softened because of their afflictions, insomuch that they did humble themselves before God, even in the depth of humility."
        )

        for ((ref, text) in texts) {
            val out = Paths.get("/var/tmp/partial.scores").let { Files.newBufferedWriter(it, Charsets.UTF_8) }
            val line = (ref + "\t" + processor.process(text))
            save(out, line)
        }
    }

    private fun save(writer: BufferedWriter, line: String) {
        println(line)
        writer.write(line)
        writer.newLine()
        writer.flush()
    }

}