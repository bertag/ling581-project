# LING 581 Project: Sentiment Analysis of the Book of Mormon

This repository contains the code and raw data used in my LING 581 Final Project.  It is written in Kotlin and can be compiled and run using Maven and JDK 8+ (tested using OpenJDK 11).  Note that the main method of this project doesn't do anything useful.  Instead, I created a series of "tests" in `src/test/kotlin` to perform different sentiment analysis variations.

The `ling581-project-data.ods` file contains the raw result data from the project and `questionnaire-responses.ods` contains the responses to the questionnaire.  The questions themselves can be viewed at `questionnaire-prompts.md`.  A customized sentiment model is included at `src/main/resources/model.ser.gz`.

## Retraining Sentiment Model

Use the following regular expression to wrap each token of a sentence in in the PTB tree format (with an initial neutral sentiment assigned to each word: `s/\(\w\+\|'\|,\|;\|\.\)/(2 \1)/g`

Here are some examples of sentences added to the default model to better facilitate processing of scriptural text:

```
(2 (0 (1 (2 (2 For) (2 (2 (2 (2 it) (2 is)) (2 because)) (2 that))) (1 (2 (2 ye) (2 are)) (1 (1 (2 cast) (2 out)) (2 ,)))) (0 (0 (2 that) (0 (0 (2 ye) (0 (2 are) (0 despised))) (2 (2 of) (3 (2 your) (3 brethren))))) (0 (2 (2 because) (2 of)) (0 (2 your) (0 (0 (3 exceeding)(1 poverty)) (2 ,)))))) (2 (2 (2 that) (2 (2 ye) (2 (2 are) (2 (2 brought) (2 to))))) (3 (2 a) (3 (1 lowliness) (2 (2 of) (3 (3 heart) (2 .)))))))
(1 (2 (2 For) (2 (2 ye) (2 (2 are) (2 necessarily)))) (1 (2 brought) (3 (2 to) ( 3(3 (2 be) (3 humble)) (2 .)))))
(0 (0 (0 (0 (2 (2 And) (2 (2 there) (2 (2 had) (2 been)))) (0 murders)) (2 ,)) (1 (1 (1 (2 and) (1 contentions)) (2 ,)) (1 (1 (2 and) (1 dissensions)) (2 ,)))) (0 (2 and) (0 (0 (0 (0 (2 (2 all) (2 (2 manner) (2 of))) (1 iniquity)) (2 among)) (3 (2 the) (3 (2 (2 people) (2 of)) (2 Nephi)))) (2 .))))
(3 (3 (3 (2 Nevertheless) (3 (2 for) (3 (2 the) (3 (3 righteous) (2 sake))))) (2 ,)) (3 (2 (2 yea) (2 ,)) (3 (4 (4 (2 (2 because) (2 of)) (4 (2 the) (4 (4 prayers) (3 (2 of) (3 (2 the) (3 righteous)))))) (2 ,)) (3 (3 (2 they) (3 (2 were) (2 spared))) (2 .)))))
(1 (2 (2 (2 But) (2 behold)) (2 ,)) (1 (1 (1 (1 (1 (2 (2 (2 because) (2 of)) (2 the)) (1 (2 exceedingly) (1 (2 (2 (2 great) (2 length)) (2 (2 of) (2 the))) (0 war)))) (2 (2 between) (2 (3 (2 the) (3 Nephites)) (1 (2 and) (1 (2 the) (1 Lamanites)))))) (0 (2 many) (0 (2 had) (0 (2 become) (0 hardened))))) (2 ,)) (1 (1 (2 (2 (2 because) (2 of)) (2 the)) (1 (2 exceedingly) (1 (2 (2 (2 great) (2 length)) (2 (2 of) (2 the))) (0 war)))) (2 .))))
(3 (3 (3 (2 And) (3 (3 (2 many) (3 (2 were) (3 softened))) (1 (2 (2 because) (2 of)) (1 (2 their) (1 afflictions))))) (2 ,)) (3 (3 (3 (2 (2 insomuch) (2 that)) (3 (3 (2 (2 they) (2 did)) (3 (3 humble) (2 themselves))) (3 (2 before) (3 God)))) (2 ,)) (3 (3 (2 even) (3 (2 (2 in) (2 the)) (3 (3 (3 depth) (2 of)) (3 humility)))) (2 .))))
```

## License

As per the license of CoreNLP, upon which this project is based, the primary license of this project is [GPLv3](http://www.gnu.org/licenses/gpl-3.0.html).  A few mathematical functions were sourced from http://rosettacode.org, and these methods are licensed [GFDL-1.2](http://www.gnu.org/licenses/fdl-1.2.html).  The source text for the Book of Mormon is available in the public domain and was sourced from [Project Gutenberg](http://www.gutenberg.org/ebooks/17).  I have included with this project a version that excludes the Project Gutenberg preface and suffix, which is allowed per the public domain nature of the work and required as a pre-processing step to my NLP pipeline.
