# Sentiment Analysis of The Book of Mormon

This questionnaire is part of a project being conducted by Scott Bertagnole as part of LING 581, to evaluate the performance of sentiment analysis of The Book of Mormon.  This questionnaire will provide human responses that will be used for comparison with the sentiment scores being produced by the system being evaluated.

This questionnaire consists of 9 questions and will take approximately 10 minutes to complete.  There are minimal risks for participation in this study, and you will not be paid for your compensation.

Involvement in this study is voluntary.  You may refuse to participate entirely and you may withdraw at any time without penalty.  There will be no reference to your identity at any point, and your responses to the questionnaire will be anonymous.  The completion of this questionnaire implies your consent to participate.

If you have questions regarding this study, you may contact me at [redacted], or you may contact my professor [redacted].  If you have any questions or concerns regarding your rights as a participant in research projects, contact the IRB Administrator at A-285 ASB, Brigham Young University, Provo UT 84602, by email at [redacted], or by phone at [redacted].

## General Sentiment Rating

Read the provided passage from The Book of Mormon and rate it using the following sentiment scale:

1. Very Negative
2. Negative
3. Neutral
4. Positive
5. Very Positive

Rate the following passage:

> And it came to pass in the thirty and sixth year, the people were all converted unto the Lord, upon all the face of the land, both Nephites and Lamanites, and there were no contentions and disputations among them, and every man did deal justly one with another.

Rate the following passage:

> But behold, I am consigned that these are my days, and that my soul shall be filled with sorrow because of this the wickedness of my brethren.

Rate the following passage:

> Now it came to pass that while Amalickiah had thus been obtaining power by fraud and deceit, Moroni, on the other hand, had been preparing the minds of the people to be faithful unto the Lord their God.

Rate the following passage:

> For behold, ye shall be as a whale in the midst of the sea. for the mountain waves shall dash upon you. Nevertheless, I will bring you up again out of the depths of the sea. for the winds have gone forth out of my mouth, and also the rains and the floods have I sent forth.

Rate the following passage:

> And now, my sons, remember, remember that it is upon the rock of our Redeemer, who is Christ, the Son of God, that ye must build your foundation. that when the devil shall send forth his mighty winds, yea, his shafts in the whirlwind, yea, when all his hail and his mighty storm shall beat upon you, it shall have no power over you to drag you down to the gulf of misery and endless wo, because of the rock upon which ye are built, which is a sure foundation, a foundation whereon if men build they cannot fall.

Rate the following passage:

> And it came to pass that I did arm them with bows, and with arrows, with swords, and with cimeters, and with clubs, and with slings, and with all manner of weapons which we could invent, and I and my people did go forth against the Lamanites to battle.

## Partial Sentiment Rating

Read the following passage fragments and rate each one independently of the others using the following sentiment scale:

1. Very Negative
2. Negative
3. Neutral
4. Positive
5. Very Positive

Afterwards, evaluate the overall sentiment of entire passage as a whole.

Evaluate the following fragment in isolation:

> I say unto you, it is well that ye are cast out of your synagogues, that ye may be humble, and that ye may learn wisdom. for it is necessary that ye should learn wisdom.

Evaluate the following fragment in isolation:

> For it is because that ye are cast out, that ye are despised of your brethren because of your exceeding poverty, that ye are brought to a lowliness of heart.

Evaluate the following fragment in isolation:

> For ye are necessarily brought to be humble.

Evaluate the entire passage as a whole:

> I say unto you, it is well that ye are cast out of your synagogues, that ye may be humble, and that ye may learn wisdom. for it is necessary that ye should learn wisdom; for it is because that ye are cast out, that ye are despised of your brethren because of your exceeding poverty, that ye are brought to a lowliness of heart; for ye are necessarily brought to be humble.

----

Evaluate the following fragment in isolation:

> And there had been murders, and contentions, and dissensions, and all manner of iniquity among the people of Nephi.

Evaluate the following fragment in isolation:

> Nevertheless for the righteous’ sake, yea, because of the prayers of the righteous, they were spared.

Evaluate the following fragment in isolation:

> But behold, because of the exceedingly great length of the war between the Nephites and the Lamanites many had become hardened, because of the exceedingly great length of the war;

Evaluate the following fragment in isolation:

> And many were softened because of their afflictions, insomuch that they did humble themselves before God, even in the depth of humility.

Evaluate the entire passage as a whole:

> And there had been murders, and contentions, and dissensions, and all manner of iniquity among the people of Nephi; nevertheless for the righteous’ sake, yea, because of the prayers of the righteous, they were spared. But behold, because of the exceedingly great length of the war between the Nephites and the Lamanites many had become hardened, because of the exceedingly great length of the war; and many were softened because of their afflictions, insomuch that they did humble themselves before God, even in the depth of humility.

## Relative Scoring

The task on this page is different than the previous questions.  Rather than try to assign an independent sentiment rating to each passage, instead rank them RELATIVE TO EACH OTHER.

Rank the following passage relative to the others:

> And it came to pass that after I, Nephi, had been carried away in the spirit, and seen all these things, I returned to the tent of my father.

Rank the following passage relative to the others:

> And thus ended the thirtieth year of the reign of the judges over the people of Nephi. Moroni and Pahoran having restored peace to the land of Zarahemla, among their own people, having inflicted death upon all those who were not true to the cause of freedom.

Rank the following passage relative to the others:

> And now it came to pass that when Jesus had ended these sayings he cast his eyes round about on the multitude, and said unto them: Behold, ye have heard the things which I taught before I ascended to my Father. therefore, whoso remembereth these sayings of mine and doeth them, him will I raise up at the last day.

Rank the following passage relative to the others:

> And it came to pass that they threw down their weapons of war, and they would not take them again, for they were stung for the murders which they had committed. and they came down even as their brethren, relying upon the mercies of those whose arms were lifted to slay them.

Rank the following passage relative to the others:

> But thus saith the Lord God: O fools, they shall have a Bible. and it shall proceed forth from the Jews, mine ancient covenant people. And what thank they the Jews for the Bible which they receive from them? Yea, what do the Gentiles mean? Do they remember the travails, and the labors, and the pains of the Jews, and their diligence unto me, in bringing forth salvation unto the Gentiles?
